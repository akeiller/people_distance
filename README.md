The config.json contains three values:
filters, list of strings: This comes from https://www.alwaysai.co/model-catalog?model=alwaysai/ssd_mobilenet_v2_oidv4, and the model will only recognize objects from this list.
calibration, float: A value that distance is multiplied by if the realsense is miscalibrated.
using_recording, boolean: If the code will get frames from the recording.bag file or the plugged in realsense camera.