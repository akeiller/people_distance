import edgeiq

if __name__ == "__main__":
    import json
    import pyrealsense2 as rs
    import distance_functions as df
    import numpy as np
    import cv2
    
    # Read input from files
    user_config = json.load(open('config.json'))
    filter_labels = user_config['filters']
    using_recording = user_config['using_recording']
    calibration = user_config['calibration']

    # Start streaming
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    if using_recording:
        config.enable_device_from_file('recording.bag', True)
    pipeline.start(config)
    streamer = edgeiq.Streamer().setup()

    # Set up object detection
    obj_detect = edgeiq.ObjectDetection('alwaysai/ssd_mobilenet_v2_oidv4')
    obj_detect.load(engine=edgeiq.Engine.DNN)

    # Streaming loop
    try:
        while True:
            # Get frames
            frames = pipeline.wait_for_frames()
            depth = frames.get_depth_frame()
            color_image = np.asanyarray(frames.get_color_frame().get_data())

            # Get results
            results = obj_detect.detect_objects(color_image, confidence_level=.2)

            # Leave filter_labels blank for no filtering
            if filter_labels:
                filtered_predictions = edgeiq.filter_predictions_by_label(results.predictions, filter_labels)
            else:
                filtered_predictions = results.predictions

            # Put boxes around detected objects
            marked_up_color_image = edgeiq.markup_image(color_image, filtered_predictions)

            # Find distance and mark up image further
            if len(filtered_predictions) > 1:
                # Loop through every pair of points
                for i in range(len(filtered_predictions)):
                    for j in range(i + 1, len(filtered_predictions)):
                        # Get object centers
                        center1 = filtered_predictions[i].box.center
                        center2 = filtered_predictions[j].box.center

                        # Adjusts so they are the center of the objects on the depth camera
                        fix_center1 = df.fix_FOV(center1)
                        fix_center2 = df.fix_FOV(center2)

                        # Using the realsense data get distance from camera
                        distance1 = depth.get_distance(fix_center1[0], fix_center1[1])
                        distance2 = depth.get_distance(fix_center2[0], fix_center2[1])

                        # Using the centers distance in pixels get the angle
                        angle = df.pixels_to_angle(df.find_distance(fix_center1, fix_center2))
                        # Using distance from camera and angle get distance between objects
                        distance = df.angle_to_distance(angle, distance1, distance2)*calibration
                        # Make a color from those objects close is red and far is green
                        color = df.get_color(distance, 2)
                        # Get the location of the rectangle to draw
                        rectangle = df.find_rectangle(center1, center2)

                        # Draw the line between objects
                        marked_up_color_image = cv2.line(marked_up_color_image, (int(center1[0]), int(center1[1])),
                                                         (int(center2[0]), int(center2[1])), color, 3)

                        # Draw rectangle
                        marked_up_color_image = cv2.rectangle(marked_up_color_image, (rectangle[0], rectangle[1]),
                                                              (rectangle[2], rectangle[3]), color, -1)

                        # Draw text in rectangle
                        marked_up_color_image = cv2.putText(marked_up_color_image, str(round(distance,2)),
                                                            (rectangle[0], rectangle[1]), cv2.FONT_HERSHEY_SIMPLEX, 1,
                                                            (255, 255, 255), 3)
            # Display image
            streamer.send_data(marked_up_color_image, 'This will be printed on the Streamer')
            if streamer.check_exit():
                break
    finally:
        # Stop camera and streamer
        streamer.close()
        pipeline.stop()
