import math


# Compute ange from pixels
def pixels_to_angle(pixels):
    return pixels * 99 / 800


# Compute distance between objects from angle and distances from camera
def angle_to_distance(angle, distance1, distance2):
    return math.sqrt(
        distance1 * distance1 + distance2 * distance2 - 2 * distance1 * distance2 * math.cos(angle * 0.01745329252))


# Account and fix the FOV difference between the color and depth sensors
def fix_FOV(cord):
    return [int(2 * (cord[0] - 320) / 3 + 320), int(2 * (cord[1] - 240) / 3 + 240)]


# Get the pixel distance between two points
def find_distance(cord1, cord2):
    a = cord1[0] - cord2[0]
    b = cord1[1] - cord2[1]
    return math.sqrt(a * a + b * b)


# turn distance into color for visualization purposes
def get_color(distance, max_distance):
    return 0, distance * 255 / max_distance, 255 - distance * 255 / max_distance


# get midpoint of two points
def find_rectangle(point1, point2):
    midpoint = int((point1[0] + point2[0]) / 2), int((point1[1] + point2[1]) / 2)
    return midpoint[0] - 35, midpoint[1] + 14, midpoint[0] + 35, midpoint[1] - 14
