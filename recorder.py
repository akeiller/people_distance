import pyrealsense2 as rs

frame_count = int(input("how many frames do you want(30 fps): "))

pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
config.enable_record_to_file('recording.bag')
pipeline.start(config)

try:
    for i in range(frame_count):
        frames = pipeline.wait_for_frames()
finally:
    pipeline.stop()